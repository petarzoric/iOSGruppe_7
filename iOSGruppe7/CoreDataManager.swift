//
//  CoreDataManager.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 21.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

/*
 Alles was mit Core-Data zu tun hat, steht hier.
 Speichert die Requests und das Array für die Präferenzen.
 Core Data wird in unserem Fall benutzt, um die Präferenzen des Users speichern zu können.
 
 Die Klasse bietet FUnktionalitäten fürs Hinzufügen, Anzeigen und Löschen von Einträgen
 */

import Foundation
import CoreData
import UIKit


class CoreDataManager {
    
    var appDelegate: AppDelegate
    var context: NSManagedObjectContext
    var newPreference: NSManagedObject
    var request: NSFetchRequest<NSFetchRequestResult>
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "UserPreferences"))
    var preferenceArray = [String]()
    
    
    init(entityName: String) {
        appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        print("initialized the core data stuff")
        newPreference = NSEntityDescription.insertNewObject(forEntityName: entityName, into: context)
        request = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPreferences")
        request.returnsObjectsAsFaults = false
        self.updateArray()
        
        
    }
    /*
     Zieht die aktuellen Daten und schreibt sie ins Array, welches die User-Präferenzen speichert.
    */
    
    func updateArray(){
        preferenceArray = self.getData()
        do {
            
            try context.save()
            print("s a v e d")
            
        }
        catch {
            
        }
        
        
        showCoreData()
    }
    
    
    /*
     Cleart das ganze Array -> keine Präferenzen mehr gespeichert.
     
     */
    
    func deleteAllData()
    {
        
        do {
            try context.execute(deleteRequest)
        }
        catch {
            print(error)
        }
        print("deleted all core data")
        showCoreData()
    }
    
    /*
     Nimmt nen Key als String an, sucht nach dem Eintrag und löscht ihn, falls vorhanden
     */
    
    func deleteObjectfromDatabase(toRemove: String){
        updateArray()
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if let userPreference = result.value(forKey: "favoritePlace") as? String {
                        
                        if (result.value(forKey: "favoritePlace") as? String == toRemove){
                            context.delete(result)
                            updateArray()
                            //  break
                        }
                    }
                }
            }
            else {
                print("there is no data to show")
            }
        }
            
        catch {
            print("no data to show, catch case")
            
        }
        
    }
    

    /*
     "Alternative" verwirrt ein bisschen. 
     Gab ursprünglich mal add-Methoden, diese hier hat sich durchgesetzt.
     Liebe Grüße an XCode an dieser Stelle, simples Refactoring klappt ja nicht.
     */
    
    func alternativeAdd(toAdd: String){
        let object = NSManagedObject(entity: NSEntityDescription.entity(forEntityName: "UserPreferences", in: context)!, insertInto: context)
        object.setValue(toAdd, forKey: "favoritePlace")
        context.insert(object)
        
    }
    
    func showCoreData(){
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    if let userPreference = result.value(forKey: "favoritePlace") as? String {
                        
                        print(userPreference)
                        print("--------------------")
                    }
                }
            }
            else {
                print("there is no data to show")
            }
        }
            
        catch {
            print("no data to show, catch case")
            
        }
        
    }
    
    
    
    func getData() -> [String]{
        var resultArray = [String]()
        do {
            let results = try context.fetch(request)
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    
                    if let userPreference = result.value(forKey: "favoritePlace") as? String {
                        resultArray.append(userPreference)
                        
                    }
                }
                
            }
            else {
                print("there is no data to show")
                
            }
        }
            
        catch {
            print("no data to show, catch case")
            
        }
        return resultArray
        
        
    }
    
    
    
    func showPreferenceData(){
        print("Die aktuellen User-Preferenzen sind derzeit:")
        for preference in preferenceArray {
            print(preference)
            print(" - - - - - - - - - - - - - - - ")
        }
    }
    
    
    
}
