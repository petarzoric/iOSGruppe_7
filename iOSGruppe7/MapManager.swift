//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation
import GoogleMaps
import CoreLocation
import GooglePlaces


//Verwaltung der Karte, Marker, routen und Interaktion mit der ui

class MapManager : NSObject, GMSMapViewDelegate, CLLocationManagerDelegate
{
    private static var googleMap : GMSMapView? = nil //Karte
    private var lastClickedMarker : GMSMarker = GMSMarker() //Zuletzt geklicketer marker für double click event
    private var markers = [Result : GMSMarker]() //Temporäre marker
    private var routeModes = [RouteMode]() //Navigationsmodi für routen
    private var routes = [Route]()  //Alle routen
    private var uiCallback : MapManagerProtocol? = nil //Callback auf UI
    private var selectedMarkers = [GMSMarker]() //Ausgewählte marker
    private var RouteStyleLengths : [RouteMode : [NSNumber]] = [RouteMode.Walking : [5, 5], RouteMode.Bicycle : [15, 15], RouteMode.Transit : [50, 50], RouteMode.Default : [100, 100]] //Styles für routen
    private var didTapyMyLocation = false //auf mylocationbutton geclicked
    private var includeMyLocation = false //Meine position in routenliste inkludieren
    private static var locationManager = CLLocationManager()
    private static var lastLocation = CLLocationCoordinate2D()

    
    override init()
    {
        super.init()
        MapManager.locationManager.delegate = self
        MapManager.locationManager.requestWhenInUseAuthorization()
        MapManager.locationManager.startUpdatingLocation()
    }
    
    
    //Map initialization
    private func initMap()
    {
        MapManager.googleMap!.delegate = self
        MapManager.googleMap!.isMyLocationEnabled = true
        MapManager.googleMap!.settings.myLocationButton = true
    }
    
    
    //---------------------------------Setter-------------------------------------------------------------------------
    
    //Setter aufgerufen von viewDidLoad da bei MapManager erzeugung nicht vorhanden
    func setMap(googleMap : GMSMapView)
    {
        MapManager.googleMap = googleMap
        initMap()
    }
    
    func setUiCallback(uiCallback : MapManagerProtocol)
    {
        self.uiCallback = uiCallback
    }
    
    func setIncludeMyLocation(include : Bool)
    {
        includeMyLocation = include
        drawRouteForMultiplePoints(markersList: selectedMarkers)
        
    }
    
    //--------------Location Manager----------------------------------------------
    
    internal func locationManager(_: CLLocationManager, didUpdateLocations: [CLLocation])
    {
        let newLocation = didUpdateLocations.first!
        let distance = newLocation.distance(from: Tools.coordinateToLocation(coordinate: MapManager.lastLocation))

        
        
        MapManager.lastLocation = newLocation.coordinate
    }
    
    public static func getCoordinate() -> CLLocationCoordinate2D
    {
        return (MapManager.googleMap?.myLocation?.coordinate)!
        
    }
    
    
    
    
    
    //--------------GMSMapViewDelegate--------------------------------------------
    
    
    //GMSMapviewDelegate
    internal func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        MapManager.googleMap?.isMyLocationEnabled = true
    }
    
    //Auf Lokalisierungsbutton geclicked
    internal func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        if !didTapyMyLocation
        {
            didTapyMyLocation = true
        }
        else
        {
            
        }
        includeMyLocation = true
        uiCallback?.onMyLocationButtonClicked()
        drawRouteForMultiplePoints(markersList: selectedMarkers)
        return false
    }
    
    internal func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        MapManager.googleMap?.isMyLocationEnabled = true
        if (gesture){
            mapView.selectedMarker = nil
        }
    }
    
    
    //Click auf marker
    internal func mapView(_ mapView : GMSMapView, didTap marker : GMSMarker) -> Bool
    {
        if lastClickedMarker.isEqual(marker)
        {
            onMarkerDoubleClick(marker: marker)
        }
        lastClickedMarker = marker

        return false;
    }
    

    
    //Click auf infofenster von Marker
    internal func mapView(_ mapView : GMSMapView, didTapInfoWindowOf marker : GMSMarker)
    {
        if selectedMarkers.contains(marker)
        {
            return
        }
        
        MapManager.googleMap!.clear()
        
        clearMarkers()
        addSelectedMarker(marker: marker)
    }
    
    //Longclick eventhandler
    internal func mapView(_ mapView : GMSMapView, didLongPressAt coordinate : CLLocationCoordinate2D)
    {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate, completionHandler: {response, error in
            if error == nil
            {
                let result = Result(location : coordinate, name: (response?.firstResult()?.addressLine1()!)!, placeId: "",  addressName: (response?.firstResult()?.addressLine2())!, imageURL: "")
                let marker = result.addToMapAsMarker(mapView : MapManager.googleMap!)
                self.addMarker(result: result)
                MapManager.googleMap!.selectedMarker = marker
            }
            else
            {
                print(error!)
            }
        })
    }
    
    
    //POI geklickt
    internal func mapView(_ mapView : GMSMapView, didTapPOIWithPlaceID placeID:  String, name : String, location : CLLocationCoordinate2D)
    {
        //Extra code weil daten teilweise schon vorhanden
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(location, completionHandler: {response, error in
            if error == nil
            {
                let result = Result(location : location, name: name, placeId: placeID, addressName: (response?.firstResult()?.addressLine1())!, imageURL: "")
                let marker = result.addToMapAsMarker(mapView : MapManager.googleMap!)
                self.addMarker(result: result)
                MapManager.googleMap!.selectedMarker = marker
            }
            else
            {
                print(error!)
            }
            
        })

    }
    

    //---------------------------Mapmanager Methoden---------------------------------------------------------------------------------------------------------
    
    //Click auf listview
    func onFilterSelected(item : GMSMarker)
    {
        let camera = GMSCameraPosition.camera(withLatitude: item.position.latitude, longitude: item.position.longitude, zoom: 6.0)
        MapManager.googleMap = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
    }
    
    //2Fach klick auf einen Marker
    func onMarkerDoubleClick(marker : GMSMarker)
    {
        addSelectedMarker(marker: marker)
        //uiCallback!.markerSelected(marker: marker, correspondingResult : marker.userData as! Result)
    }
    
    //Tupelliste für start und endpunkte für route erstellen
    private func generateStartEndTuples(markersList : [GMSMarker], navigationModes : [RouteMode]) -> [(start :  CLLocationCoordinate2D, end : CLLocationCoordinate2D, startColor : UIColor, endColor : UIColor, routeMode : RouteMode)]
    {
        var tuples = [(start :  CLLocationCoordinate2D, end : CLLocationCoordinate2D, startColor : UIColor, endColor : UIColor, routeMode : RouteMode)]()
        
        if markersList.count == 0
        {
            return []
        }
        if markersList.count == 1
        {
            if !includeMyLocation
            {
                return [] // Keine route zwischen meinem punkt
            }
            
            let firstMarker = markersList.first!
            let startColor = includeMyLocation ? Tools.colorFromRGBInt(r : 66, g : 133, b : 244) : UIColor.green
            let endColor = includeMyLocation ? Tools.deterministicUIColor(value: (firstMarker.userData as! Result).hashValue) : UIColor.red
            
            return [(start : MapManager.getCoordinate(), end : firstMarker.position, startColor : startColor, endColor : endColor, routeMode : navigationModes[0])]
        }
        else if markersList.count == 2
        {
            if includeMyLocation
            {
                let firstMarker = markersList.first!
                let secondMarker = markersList.last!
                
                let endColor = UIColor.red
                
                tuples.append((start : MapManager.getCoordinate(), end : firstMarker.position, startColor : Tools.colorFromRGBInt(r : 66, g : 133, b : 244), endColor : UIColor.green, routeMode : navigationModes.first!))
                tuples.append((start : firstMarker.position, end : secondMarker.position, startColor : UIColor.green, endColor : endColor, routeMode : navigationModes.last!))
            }
            else
            {
                let firstMarker = markersList.first!
                let secondMarker = markersList.last!
                
                tuples.append((start : firstMarker.position, end : secondMarker.position, startColor : UIColor.green, endColor : UIColor.red, routeMode : navigationModes.first!))
            }
            
        }
        else
        {
            var startIndex = 0
            
            if includeMyLocation //Standrort inkludieren
            {
                let firstMarker = markersList.first!
                let startColor = Tools.colorFromRGBInt(r : 66, g : 133, b : 244)
                let endColor = Tools.deterministicUIColor(value: (firstMarker.userData as! Result).hashValue)
                
                tuples.append((start : MapManager.getCoordinate(), end : firstMarker.position, startColor : startColor, endColor : endColor, routeMode : navigationModes[0])) //Standort -> Erster marker
            }
            else
            {
                startIndex = 1
                let firstMarker = markersList.first!
                let secondMarker = markersList[1]
                
                let startColor = UIColor.green
                let endColor = Tools.deterministicUIColor(value: (firstMarker.userData as! Result).hashValue)
                
                tuples.append((start : firstMarker.position, end : secondMarker.position, startColor : startColor, endColor : endColor, routeMode : navigationModes[0])) //Erster -> 2. Marker
            }
            
            for i in startIndex..<(markersList.count - 2) //0...c-2 wenn MyLocation ansonsnten 1...c-2
            {
                let start = markersList[i]
                let end = markersList[i + 1]
                
                let startColor = Tools.deterministicUIColor(value: (start.userData as! Result).hashValue)
                let endColor = Tools.deterministicUIColor(value: (end.userData as! Result).hashValue)
                
                tuples.append((start : start.position, end : end.position, startColor : startColor, endColor : endColor, routeMode : navigationModes[i]))
            }
            
            let secondLastMarker = markersList[markersList.count - 2]
            let lastMarker = markersList.last!
            
            let secondLastColor = Tools.deterministicUIColor(value: (secondLastMarker.userData as! Result).hashValue)
            let lastColor = UIColor.red
            
            tuples.append((start : secondLastMarker.position, end : lastMarker.position, startColor : secondLastColor, endColor : lastColor, routeMode : navigationModes.last!))
        }
        
        return tuples
    }
    

    

    
    //Alles zurücksetzen
    public func clearAll()
    {
        MapManager.googleMap!.clear()
        markers.removeAll()
        clearRoutes()
    }
    
    
    
    //----------------------Marker funktionen---------------------------------------------------------------------------------------------------------
    
    
    //Marker löschen (ausgewählte werden beibehalten)
    private func clearMarkers()
    {
        for (_, marker) in markers
        {
            marker.map = nil
        }
        markers.removeAll()
    }
    
    //Ausgewählte marker anzeigen und icons aktualisieren
    //Marker auswählen -> alles löschen -> nur ausgewählte marker anzeigen
    private func showSelectedMarkers()
    {
        var count = 0
        
        for marker in selectedMarkers
        {
            marker.map = MapManager.googleMap
            
            if count == 0
            {
                marker.icon = UIImage(named : "MarkerStart")
            }
            else if count == selectedMarkers.count - 1
            {
                marker.icon = UIImage(named : "MarkerEnd")
            }
            else
            {
                let result = marker.userData as! Result
                marker.icon = GMSMarker.markerImage(with: Tools.deterministicUIColor(value: result.hashValue))
            }
            
            count += 1
        }
        clearRoutes()
        drawRouteForMultiplePoints(markersList : selectedMarkers)
        buildBounds()
    }
    
    //marker anhand eines googleapi ergebnisses zur map hinzufügen
    private func addMarker(result : Result)
    {
        let marker = result.addToMapAsMarker(mapView : MapManager.googleMap!)
        markers[result] = marker
    }
    
    
    private func addSelectedMarker(marker : GMSMarker)
    {
        selectedMarkers.append(marker)
        MapManager.googleMap!.selectedMarker = marker
        uiCallback!.markerSelected(marker: marker, correspondingResult : marker.userData as! Result)
        showSelectedMarkers()
        //routeModes.append(RouteMode.Default)//Evtl entfernen, ansonsten nil bei 3 marker routenzeichnung
    }
    
    //Ergebnis von autovervollständigung auf karte anzeigen
    internal func addAutoCompleteResult(placeResult : GMSPlace) -> GMSMarker
    {
        let camera = GMSCameraPosition.camera(withLatitude: placeResult.coordinate.latitude, longitude: placeResult.coordinate.longitude, zoom: 15.0)
        MapManager.googleMap!.camera = camera
        
        clearMarkers()
        
        let result = Result.fromGMSPlace(place: placeResult)
        let marker = result.addToMapAsMarker(mapView: MapManager.googleMap!)
        //markers[result] = marker
        addSelectedMarker(marker: marker)
        
        return marker
    }
    
    
    
    //In filterview geladene ergebnisse anzeigen
    public func displayGooglePlacesResult(results : [Result])
    {
        clearMarkers()
        
        for result in results
        {
            let marker = result.addToMapAsMarker(mapView : MapManager.googleMap!)
            markers[result] = marker
        }
        if results.count > 1
        {
            buildBoundsForTempMarker()
        }
    }
    
    //In filterview geladene ergebnisse anzeigen
    public func displayGooglePlacesResult(resultsKVP : [String : [Result]])
    {
        clearMarkers()
        
        for (_, results) in resultsKVP
        {
            for result in results
            {
                let marker = result.addToMapAsMarker(mapView : MapManager.googleMap!)
                markers[result] = marker
            }
        }
    }
    
    

    
    //----------------------Routen Funktionen---------------------------------------------------------------------------------------------------------
    
    //Routen tauschen wenn zugehörige items vertauscht werden
    public func swapRouteModes(source : Int, dest : Int)
    {
        swap(&routeModes[source], &routeModes[dest])
    }
    
    //Gezeichnete routen löschen
    private func clearRoutes()
    {
        for route in routes
        {
            route.mapsRoute.map = nil
        }
        routes.removeAll()
    }
    
    //routen für alle punkte zeichnen
    private func drawRouteForMultiplePoints(markersList : [GMSMarker])
    {
        clearRoutes()
        var callbackCount = 0
        for tuple in generateStartEndTuples(markersList: markersList, navigationModes: routeModes)
        {
            DirectionsAPI.loadGoogleDirection(startLocation: tuple.start, endLocation: tuple.end, routeMode: tuple.routeMode, routesLoadedCallback: {(routes: [Route]) in
                
                for route in routes
                {
                    let mapRoute = route.addToMap(map : MapManager.googleMap!)
                    let styles = [GMSStrokeStyle.solidColor(tuple.startColor), GMSStrokeStyle.solidColor(tuple.endColor)]
                    mapRoute.spans = GMSStyleSpans(mapRoute.path!, styles, self.RouteStyleLengths[tuple.routeMode]!, GMSLengthKind.rhumb)
                    
                    self.routes.append(route)
                }
                
                if callbackCount >= markersList.count - 2 //-1 Index, -1 (3 Punkte 2 Routen)
                {
                    self.uiCallback!.routesLoaded(routes: self.routes)
                }
                callbackCount += 1
            })
        }

    }
    
    //Routenpunkte setzen und zeichnen
    public func setRoute(markers : [GMSMarker])
    {
        for selectedMarker in selectedMarkers
        {
            selectedMarker.map = nil
        }
        selectedMarkers.removeAll()
        clearAll()
        
        if markers.count == 0
        {
            return
        }
        
        for marker in markers
        {
            selectedMarkers.append(marker)
        }
        showSelectedMarkers()
    }
    
    //Updaten
    public func setLastRouteMode(routeMode : RouteMode)
    {
        routeModes[routeModes.count - 1] = routeMode
        showSelectedMarkers()
    
    }
    
    //Hinzufügen
    public func addLastRouteMode(routeMode : RouteMode)
    {
        routeModes.append(routeMode)
        showSelectedMarkers()
    }
    
    public func removeRouteMode(index : Int)
    {
        routeModes.remove(at: index)
    }


    
    //----------------------Maps Funktionen---------------------------------------------------------------------------------------------------------
    
    //Callback wenn routen geladen, jetzt die kürzeste bestimmen
    private func allRoutesCalculated(routesForPermutation : [[Route]], permutatedMarkers : [[GMSMarker]])
    {
        var shortestRoutesIndex = Int.max
        var shortestDistance = Double.infinity
        var currentIndex = 0
        
        for routePermutation in routesForPermutation
        {
            var distance : Double = 0
            for route in routePermutation
            {
                distance += route.distanceDouble()
            }
            if distance < shortestDistance && distance != 0
            {
                shortestRoutesIndex = currentIndex
                shortestDistance = distance
            }
            currentIndex += 1
        }

        clearAll()
        clearMarkers()
        clearRoutes()
        MapManager.googleMap?.clear()
        
        selectedMarkers = permutatedMarkers[shortestRoutesIndex]
        
        showSelectedMarkers()
        
        uiCallback!.shortestRoutesCalculated(marker: selectedMarkers)
    }
    
    //kürzeste distanz zwischen den punkten berechnen
    public func calculateShortestRoute()
    {
        let permutatedMarkers = permutations(xs: selectedMarkers)
        let permutatedRouteModes = permutations(xs: routeModes)
        
        var routesForPermutation : [[Route]] = [[]]
        var callbacksCalled = 0
        var currentRoute = 0
        
        let allCallbacksCount = permutatedMarkers.count * (selectedMarkers.count - 1) - 1 //== permutations * selected.Count - (selected.Count *1) => weil zwischen 3 punkten 2 routen liegen

        
        for permutation in permutatedMarkers
        {
            routesForPermutation.append([Route]())
            for tuple in generateStartEndTuples(markersList: permutation, navigationModes: permutatedRouteModes[currentRoute])
            {
                DirectionsAPI.loadGoogleDirection(startLocation: tuple.start, endLocation: tuple.end, routeMode: tuple.routeMode, forPermutationIndex : currentRoute, routesLoadedCallback: {(index : Int, routes: [Route]) in
                    routesForPermutation[index].append(contentsOf: routes)
                    callbacksCalled += 1
                    if callbacksCalled == allCallbacksCount
                    {
                        self.allRoutesCalculated(routesForPermutation: routesForPermutation, permutatedMarkers : permutatedMarkers)
                    }
                })
            }
            currentRoute += 1
        }
               //while callbacksCalled <  allCallbacksCount {
            //Warten
        //}
        
                //return permutatedMarkers[shortestRoutesIndex]
    }
    
    
    
    //Kamera positionieren um alle marker anzuzeigen
    private func buildBounds()
    {
        var bounds = GMSCoordinateBounds()

        if selectedMarkers.count == 1 && !includeMyLocation
        {
            return //Ansonsten zu nah gezoomt
        }
        
        if includeMyLocation
        {
            bounds = bounds.includingCoordinate(MapManager.getCoordinate())
        }
        
        for marker in selectedMarkers
        {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let camUpdate = GMSCameraUpdate.fit(bounds, withPadding : 60)
        MapManager.googleMap?.animate(with : camUpdate)
    }
    //Bounds für placesresult
    private func buildBoundsForTempMarker()
    {
        var bounds = GMSCoordinateBounds()
        
        if selectedMarkers.count == 1 && !includeMyLocation
        {
            return //Ansonsten zu nah gezoomt
        }
        
        if includeMyLocation
        {
            bounds = bounds.includingCoordinate(MapManager.getCoordinate())
        }
        
        for (_, marker) in markers
        {
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let camUpdate = GMSCameraUpdate.fit(bounds, withPadding : 60)
        MapManager.googleMap?.animate(with : camUpdate)
    }
}
