//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation

//Enum für Navigationsmodi
enum RouteMode : String {
    case Walking = "walking"
    case Default = "driving"
    case Bicycle = "bicycling"
    case Transit = "transit"
}
