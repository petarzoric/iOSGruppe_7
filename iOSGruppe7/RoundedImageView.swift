//
//  RoundedImageView.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 29.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedImageView: UIImageView {
    override init(image: UIImage?) {
        super.init(image: image)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.height / 1.5
        self.clipsToBounds = true
    }
}
