//
//  PreferencesViewController.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 13.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//


/*
 Der entsprechende View-Controller für die Präferenzen-Ansicht des Users, wird aufgerufen mit dem Button unten links in der initial View.
 
 */

import Foundation

import UIKit
import CoreLocation
import CoreData

class PreferencesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    //------------------------------------- OUTLETS --------------------------------------------------
    
    @IBOutlet weak var pickerTextfield: UITextField!
    
    @IBOutlet weak var secondPickerTextfield: UITextField!
    
    @IBOutlet weak var background: UIImageView!
    
    
    @IBOutlet weak var popUpView: UIView!
    
    @IBOutlet weak var PopupToolbar: UIToolbar!
    
    @IBOutlet public weak var favoriteTable: UITableView!
    
    public var picker: UIPickerView!
    
    public var secondPicker: UIPickerView!
    
    var selectedCategory: String!
    
    var selectedItem: String!
    
    var categories: [String]!
    
    var identities = [String]()
    
    public var zellen = [String]()
    
    var categoryOutput: [String]!
    
    var coreData = CoreDataManager(entityName: "UserPreferences")
    
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        popUpView.fadeOutTotal()
        
        favoriteTable.fadeIn()
    }
    
    //---------------------------------------------------------------------------------------------
    
    
    
    //---------------------------------- DIDLOAD / DIDAPPER --------------------------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        identities = ["A", "B", "C", "D"]
        //secondPicker.delegate = self
        
        //pickerTextfield.delegate = self
        popUpView.isHidden = true
        createPicker()
        createSecondPicker()
        createToolbar()
        createSecondToolbar()
        hideTextfield()
        categories = [
            "Bank",
            "Supermarkt",
            "Tankstellen"
        ]
        
        updateTable()
        
        favoriteTable.backgroundView = background
        
        pickerTextfield.dropShadow()
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        updateTable()
        selectedCategory = categories[0]
      //  selectedItem =
        if (self.zellen.count > 0){
            print("Tabelle ist nicht leer, hat \(zellen.count) Einträge")
        }
        else {
            print("Tabelle ist leer")
        }
        
        
        
        
    }
    
    
    
    
    //-------------------------------------------------------------------------------------------
    
    
    
    //----------------------------------- TABLEVIEW -------------------------------------------
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (zellen.count)
        
    }
    
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DynamicViewControllerCell
        let clicked = zellen[indexPath.row]
        cell.myLabel.text = zellen[indexPath.row]
        cell.myImage.image = UIImage(named: ImageDictionary.imageDictionary[clicked]!)
        cell.myLabel.text = clicked
        
        return(cell)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            let removeMe = zellen[indexPath.row]
            coreData.deleteObjectfromDatabase(toRemove: removeMe)
            print("deleted")
            self.picker.reloadAllComponents()
            self.picker.reloadInputViews()
            updateTable()
            
            
        }
    }
    
    
    
    
    
    public func getTable()-> UITableView{
        return favoriteTable
    }
    
    
    
    func addFavoriteFinal(toAdd: String){
        //coreData.addEntry(toAdd: toAdd)
        coreData.alternativeAdd(toAdd: toAdd)
        coreData.updateArray()
        coreData.showCoreData()
        updateTable()
        
        
        
    }
    
    func updateTable(){
        
        coreData.updateArray()
        zellen = coreData.preferenceArray
        zellen.sort()
        favoriteTable.reloadData()
        //reloadpickers()
        
    }
    
    
    
    @IBAction func addFavorite(_ sender: Any) {
        popUpView.alpha = 0.0
        popUpView.isHidden = false
        popUpView.fadeIn()
        popUpView.clipsToBounds = true
        PopupToolbar.clipsToBounds = true
        print("Tapped")
        favoriteTable.reloadData()
        favoriteTable.fadeOut()
        popUpView.dropShadow()
        
        
    }
    
    //---------------------------------------------------------------------------------------------
    
    
    
    
    //------------------------------------------ PICKERS ------------------------------------------
    
    func createPicker(){
        picker = UIPickerView()
        picker.delegate = self
        pickerTextfield.inputView = picker
        picker.backgroundColor = UIColor.clear
        picker.tag = 1
        print(picker.tag)
        
        
    }
    
    
    
    func createSecondPicker(){
        secondPicker = UIPickerView()
        secondPicker.delegate = self
        secondPickerTextfield.inputView = secondPicker
        secondPicker.backgroundColor = UIColor.clear
        secondPicker.tag = 2
        print(secondPicker.tag)
        
        
    }
    
    
    
    func setNewPickerValues(){
        if (selectedCategory == "Unterhaltung"){
            categoryOutput = ["Cafes", "Kinos", "Bars"]
        }
        else {
            print(selectedCategory)
            let this = PlacesDictionary.translateIntoEnum(input: selectedCategory)[0]
            categoryOutput = PlacesDictionary.dictionary[this]
            print(categoryOutput)
            categoryOutput = updatePickerList(categoryList: categoryOutput, preferences: coreData.preferenceArray)
            self.picker.reloadAllComponents()
            self.picker.reloadInputViews()
        }
        
        
    }
    
    func updatePickerList(categoryList: [String], preferences: [String] ) -> [String]{
        coreData.updateArray()
        var categories = categoryList
        for preference in preferences {
            if (categoryList.contains(preference)){
                print("springt in den if case")
                let index = categories.index(of: preference)
                categories.remove(at: index!)
            }
        }
        return categories
        
    }
    
    
    
    func hideTextfield(){
        if( secondPickerTextfield.tag == 2) {
            secondPickerTextfield.isUserInteractionEnabled = false
            
            secondPickerTextfield.isHidden = true
            print("is hidden now")
            
        }
    }
    
    //---------------------------------------------------------------------------------------------
    
    
    
    
    //------------------------------------------ TOOLBAR ------------------------------------------
    
    func createToolbar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        toolBar.barTintColor = UIColor.lightGray
        toolBar.tintColor = .white
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(PreferencesViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        pickerTextfield.inputAccessoryView = toolBar
        
    }
    
    
    
    func createSecondToolbar(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        toolBar.barTintColor = UIColor.lightGray
        toolBar.tintColor = .white
        
        let doneButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(PreferencesViewController.dismissKeyboardAgain))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        secondPickerTextfield.inputAccessoryView = toolBar
        
        
    }
    
    
   
    
    func dismissKeyboard(){
        secondPickerTextfield.alpha = 0
        view.endEditing(true)
        secondPickerTextfield.isHidden = false
        secondPickerTextfield.fadeIn()
        secondPickerTextfield.dropShadow()
        secondPickerTextfield.isUserInteractionEnabled = true
        setNewPickerValues()
        selectedItem = categoryOutput[0]
    }
    
    
    
    func dismissKeyboardAgain(){
        view.endEditing(true)
        coreData.updateArray()
        setNewPickerValues()
        addFavoriteFinal(toAdd: selectedItem)
        secondPickerTextfield.text = nil
        pickerTextfield.text = nil
        secondPickerTextfield.fadeOutTotal()
        //secondPickerTextfield.isHidden = true
        picker.reloadAllComponents()
        
        
        
    }
    
    //---------------------------------------------------------------------------------------------
    
    
    
    
    
    //------------------------------------------ OTHER STUFF ------------------------------------------
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    
    //---------------------------------------------------------------------------------------------
    
    
}
/*
 
 Extension für die beiden Pickerviews, einfach ausgelagert.
 */

extension PreferencesViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if (pickerView.tag == 1){
            return categories.count
        }
        else {
            return categoryOutput.count
        }
        
        
    }
    
    
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView.tag == 1){
            return categories[row]
            
        }
        else {
            print("updated den picker ")
            return updatePickerList(categoryList: categories, preferences: coreData.preferenceArray )[row]
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if (pickerView.tag == 1){
            selectedCategory = categories[row]
            print(selectedCategory)
            pickerTextfield.text = selectedCategory!
        }
        else {
            selectedItem = categoryOutput[row]
            print(selectedItem)
            secondPickerTextfield.text = selectedItem!
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if (pickerView.tag == 1){
            var label: UILabel
            if let view = view as? UILabel {
                label = view
            }
            else {
                label = UILabel()
            }
            label.textColor = .black
            label.textAlignment = .center
            label.font = UIFont(name: "Menlo-Regular", size: 17)
            
            label.text = categories[row]
            return label
        }
        else {
            var label: UILabel
            if let view = view as? UILabel {
                label = view
            }
            else {
                label = UILabel()
            }
            
            label.textColor = .black
            label.textAlignment = .center
            label.font = UIFont(name: "Menlo-Regular", size: 17)
            
            label.text = categoryOutput[row]
            return label
            
        }
    }
    
    
    
    
}

public extension UIView {

    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(withDuration duration: TimeInterval = 1) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeOut(withDuration duration: TimeInterval = 1) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.7
        })
    }
    
    func fadeOutTotal(withDuration duration: TimeInterval = 1) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        })
    }
    
    
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -3.5, height: 3.5)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }

}

