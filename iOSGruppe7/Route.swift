//
//  Route.swift
//  iOSGruppe7
//
//  Created by Felix on 26.07.17.
//  Copyright © 2017 Felix Frömel. All rights reserved.
//

import Foundation
import GoogleMaps

//Speichert die Route und zugehörigen informationen
class Route
{
    var duration : String
    var distance : String
    var routeMode : RouteMode
    var mapsRoute : GMSPolyline
    
    init(duration : String, distance : String, routeMode : RouteMode, mapsRoute : GMSPolyline)
    {
        self.duration = duration
        self.distance = distance
        self.routeMode = routeMode
        self.mapsRoute = mapsRoute
    }
    
    //Distanz in double konvertieren
    func distanceDouble() -> Double
    {
        let distanceStr = distance.replacingOccurrences(of: "k", with: "").replacingOccurrences(of: "m", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: ",", with: ".")
        
        if distance.contains("km")
        {
            return Double(distanceStr)! * 1000
        }
        
        return Double(distanceStr)!
    }
    
    //der karte hinzufügen
    func addToMap(map : GMSMapView) -> GMSPolyline
    {
        mapsRoute.map = map
        return mapsRoute
    }
    
}
