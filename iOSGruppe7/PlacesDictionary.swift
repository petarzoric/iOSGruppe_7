//
//  PlacesDictionary.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 05.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import Foundation

class PlacesDictionary {
    
    
    public static var dictionary :[PlacesType : [String]] = [
        PlacesType.Car_dealer : ["bmw", "audi", "mercedes", "porsche", "vw"],
        PlacesType.Bank : ["sparkasse", "commerzbank", "münchnerbank", "raiffeisenbank", "deutsche bank", "postbank"],
        PlacesType.Gas_station : ["shell", "aral", "jet", "esso", "omv", "agip"],
        PlacesType.GroceryOrSupermarket : ["rewe", "lidl", "penny", "aldi", "edeka", "galeria kaufhof", "norma", "real", "basic"]
        
    ]
    
    var ourPlaceTypes = ["Supermarkt", "Bank", "Unterhaltung", "Tankstellen"]
    /*
     public func getDictValue(eingabe: String) -> [String] {
     return self.dictionary[PlacesType.Accounting.rawValue]
     }
     */
    
    
    
    
    static func translateIntoEnum(input: String) -> [PlacesType]{
        switch input {
        case "Supermarkt":
            return [PlacesType.GroceryOrSupermarket]
            //hier sollten wir nochmal filtern, Unterhaltung hat keine Überkategorie bei Google, entsprechend müssen wir da noch mal schauen
            
        case "Unterhaltung":
            return [PlacesType.Night_club, PlacesType.Amusement_park, PlacesType.Zoo, PlacesType.Stadium, PlacesType.Casino, PlacesType.Bowling_alley, PlacesType.Bar]
        case "Gesundheit":
            return [PlacesType.Doctor, PlacesType.Pharmacy, PlacesType.Hospital]
        
        case "Shoppen":
            return [PlacesType.Car_dealer, PlacesType.Clothing_store, PlacesType.Hardware_store, PlacesType.Jewelry_store, PlacesType.Pet_store, PlacesType.Shoe_store, PlacesType.Shopping_mall]
            
            
        case "Tankstellen":
            return [PlacesType.Gas_station]
            
        case "Bank":
            return [PlacesType.Bank]
            
        case "Bars":
            return [PlacesType.Bar]
            
        case "Kinos":
            return [PlacesType.Movie_theater]
            
        case "Discotheken":
            return [PlacesType.Night_club]
            
        case "Museen":
            return [PlacesType.Museum]
            
        case "Cafés":
            return [PlacesType.Cafe]
            
        case "UBahn-Stationen": 
            return [PlacesType.Subway_station]
        
        case "Parkplätze":
            return [PlacesType.Parking]
            
        case "Krankenhaus":
            return [PlacesType.Hospital]
            
        case "Apotheke":
            return [PlacesType.Pharmacy]
            
        case "Arzt":
            return [PlacesType.Doctor]
            
       case "Baumärkte":
            return [PlacesType.Hardware_store]
            
        case "Fitness-Studio":
            return [PlacesType.Gym]
            
        case "Taxi-Stände":
            return [PlacesType.Taxi_stand]
            
        case "Mode":
            return [PlacesType.Clothing_store]
            
        case "Schuhläden":
            return [PlacesType.Shoe_store]
            
        case "Schmuckhändler":
            return [PlacesType.Jewelry_store]
            
        case "Einkaufscenter":
            return [PlacesType.Shopping_mall]
            
        
        case "Autohändler":
            return [PlacesType.Car_dealer]
            
        case "Haustiere":
            return [PlacesType.Pet_store]
        
       
            
            
            
           
            
        //erst mal nur, um überhaupt nen Default zu haben, hier sollte eine Fehlermeldung geschmissen werden
        default:
            print("Kein Enumtype gefunden für \(input) ")
            return [PlacesType.Airport]
        }
    }
    
}
    
