//
//  ViewController.swift
//  iOSGruppe7
//
//  Created by Felix Frömel on 06.06.17.
//  Copyright © 2017 Felix Frömel. All rights reserved.
//

import Foundation

class Filter : Hashable
{
    var SearchQuery : String
    var Radius : Int
    let MAX_RADIUS = 50000;
    var IsTextSearch : Bool
    
    init(searchQuery : String, radiusInMeter : Int, isTextSearch : Bool = false) {
        SearchQuery = searchQuery
        Radius = radiusInMeter
        if Radius > MAX_RADIUS
        {
            Radius = MAX_RADIUS
        }
        IsTextSearch = isTextSearch
    }
    
    
    //Für Hashable
    var hashValue : Int
    {
        return SearchQuery.hashValue;
    }
    
    //Equatable
    static func == (val1 : Filter, val2 : Filter) -> Bool
    {
        return val1.Radius == val2.Radius && val1.SearchQuery == val2.SearchQuery;
    }
}
