//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation
import CoreLocation
import GoogleMaps
import GooglePlaces



//Werte von GooglePlacesWebAPI kapseln und als marker der map zur verfügung stellen
class Result : Hashable
{
    var location : CLLocationCoordinate2D
    var name : String = ""
    var imageUrl : String = ""
    var placeId : String = ""
    var addressName : String = ""
    var hash : Int
    
    
    
    init(location : CLLocationCoordinate2D, name : String, placeId : String, addressName : String, imageURL : String)
    {
        self.location = location
        self.name = name
        self.placeId = placeId
        self.addressName = addressName
        self.imageUrl = imageURL
        
        
        if addressName == ""
        {
            self.addressName = "Keine Adresse vorhanden"
        }
        if placeId == ""
        {
            hash = String(NSDate.timeIntervalSinceReferenceDate).hashValue
        }
        else
        {
            hash = placeId.hashValue
        }
    }
    
    //aus GMSPlaceResult erstellen
    public static func fromGMSPlace(place : GMSPlace) -> Result
    {
        return Result(location : place.coordinate, name : place.name, placeId : place.placeID, addressName : place.formattedAddress!, imageURL : "")
    }

    //Als marker der karte hinzufügen
    func addToMapAsMarker(mapView : GMSMapView) -> GMSMarker
    {
        let marker = GMSMarker()
        marker.position = location
        marker.title = name
        marker.snippet = addressName
        marker.userData = self
        marker.map = mapView
        marker.icon = GMSMarker.markerImage(with: Tools.deterministicUIColor(value: hashValue))
        return marker
    }

    
    //Distanz von mir zu marker
    var distanceString : String
    {
        let distance =  (Tools.coordinateToLocation(coordinate: location).distance(from: Tools.coordinateToLocation(coordinate: MapManager.getCoordinate())))

        let kilometer = distance / 1000
        return distance < 1000 ? "\(Int(distance))m" : "\(kilometer.roundTo(places: 2))km"//Umrechnung von meter in kilometer . Am File beginn wurde double um "round to" ergänzt.
  }

    
    var hashValue : Int
    {
        return hash
    }
    
    static func == (val1 : Result, val2 : Result) -> Bool
    {
        return val1.hashValue == val2.hashValue
    }
    
}
