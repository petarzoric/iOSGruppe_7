//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation
import CoreLocation
import GoogleMaps


//Verschiedene Helperfunktionen für (Random)Farben und Locaitons
class Tools
{
    static func randomFloat() -> CGFloat
    {
        let rnd = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let rounded = round(rnd * 10)
        //return rounded / 10
        return rounded
    }
    
    static func randomColor() -> UIColor
    {
        return UIColor(red: randomFloat(), green: randomFloat(), blue: randomFloat(), alpha: 1)
    }
    
    static func randomFloat(seed : Int) -> CGFloat
    {
        srand48(seed)
        let rnd = CGFloat(drand48())
        let rounded = round(rnd * 10)
        //return rounded / 10
        return rounded
    }
    
    static func nextRandomFloat() -> CGFloat
    {
        let rnd = CGFloat(drand48())
        let rounded = round(rnd * 10)
        //return rounded / 10
        return rounded
    }
    
    
    static func randomColor(seed : Int) -> UIColor
    {
        return UIColor(red: randomFloat(seed : seed), green: nextRandomFloat(), blue: nextRandomFloat(), alpha: 1)
    }
    
    static func deterministicUIColor(value : Int) -> UIColor
    {
        let r = (value >> 16) & 0xFF
        let g = (value >> 8)  & 0xFF
        let b = (value)       & 0xFF
        
        //return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1)
        return colorFromRGBInt(r: r, g: g, b: b)
    }
    
    static func colorFromRGBInt(r : Int, g : Int, b : Int) -> UIColor
    {
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: 1)
    }
    
    static func coordinateToLocation(coordinate : CLLocationCoordinate2D) -> CLLocation
    {
        return CLLocation(latitude : coordinate.latitude, longitude : coordinate.longitude)
    }
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
