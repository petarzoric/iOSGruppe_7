//
//  AViewController.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 12.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

/*
 Dynamic, da die vorherige View Suchanfragen mit Keywords macht.
 Es kommt durchaus vor, dass nicht immer jedes Keyword nen Treffer findet.
 Diese Keywords brauchen wir dann entsprechend nicht in der Tabelle anzeigen.
 */



import UIKit
import Foundation
import CoreLocation

class DynamicViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //OUTLETS
    
    @IBOutlet weak var showAllButton: UIButton!
    
    
    
    var identities = [String]()
    // ist nur default
    public static var  FilterListeLvl1 = ["A", "B", "C", "D"]
    //Die Resultlist wird befüllt von der vorherigen View -> ThirdViewController
    public static var resultList: [String: [Result]] = [:]
    
    
    @IBOutlet public weak var dynamicTableView: UITableView!
    /*
     Coredata, weil diese View die User-Präferenzen berücksichtigt, mehr dazu unten.
    */
    var coreData = CoreDataManager(entityName: "UserPreferences")
    
    /*
     Nimmt alle Keys und bündelt die Ergebnisse und zeigt eben alle an.
     */
    @IBAction func showAllTapped(_ sender: Any) {
        var results = [Result]()
        for (key, resultList) in DynamicViewController.resultList
        {
            results.append(contentsOf: resultList)
        }
        /*
        let keys = [String] (DynamicViewController.resultList.keys)
        for key in keys {
            let results = DynamicViewController.resultList[key]
            for result in results! {
                print(result.name)
            }
            */
            ViewController.mapManager.displayGooglePlacesResult(results: results)
            self.navigationController?.popToRootViewController(animated: true)
            

        
        
    }
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        identities = ["A", "B", "C", "D"]
        showAllButton.layer.borderColor = UIColor.white.cgColor
        dynamicTableView.contentInset = UIEdgeInsetsMake(44,0,0,0);

    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (DynamicViewController.resultList.keys.count)
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DynamicViewControllerCell
        var keys = [String] (DynamicViewController.resultList.keys)
        keys = sortKeys(keys: keys)
        let clicked = keys[indexPath.row]
        print(clicked)
        
        cell.myImage.image = UIImage(named: ImageDictionary.imageDictionary[clicked]!)
        cell.myLabel.text = clicked
        
        
        cell.labelDescription.text = "\(DynamicViewController.resultList[clicked]!.count) Treffer"
        
        return(cell)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! DynamicViewControllerCell
        
        print(currentCell.myLabel.text)
        let results = DynamicViewController.resultList[currentCell.myLabel.text!]
        for result in results! {
            print(result.name)
        }
        
        ViewController.mapManager.displayGooglePlacesResult(results: results!)
        self.navigationController?.popToRootViewController(animated: true)
        
       
    }
    
    
    /*
     Hier kommen die Userpräferenzen ins Spiel, es wurden im vorhinein alle Keys geladen, die Treffer haben.
     Diese Funktion sieht nach, ob der User eventuell Präferenzen gespeichert hat, die in den Keys enthalten sind.
     Falls ja wird diese Präferenz/dieser Key an den Anfang der Liste gepackt, um entsprechend ganz oben in der Tableview zu erscheinen.
     */
    
    func sortKeys(keys: [String])-> [String]{
        var stuff = keys
        for entry in coreData.preferenceArray {
            if stuff.contains(entry){
                var index = stuff.index(of: entry)
                stuff.rearrange(from: index!, to: 0)
            }
        }
        return stuff
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension Array {
    mutating func rearrange(from: Int, to: Int) {
        insert(remove(at: from), at: to)
    }
}

