//
//  ImageDictionary.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 29.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import Foundation
class ImageDictionary {
    public static var imageDictionary: [String: String] =
        [
            "rewe" : "rewe.jpeg",
            "lidl" : "lidl.jpeg",
            "penny" : "penny.jpeg",
            "aldi" : "aldi.jpeg",
            "edeka" : "edeka.jpeg",
            "galeria kaufhof" : "galeriakaufhof.jpeg",
            "norma" : "norma.jpeg",
            "real" : "real.jpeg",
            "basic" : "basic.jpeg",
            "sparkasse" : "sparkasse.jpeg",
            "commerzbank" : "commerzbank.jpeg",
            "münchnerbank" : "münchnerbank.jpeg",
            "raiffeisenbank" : "raiffeisenbank.jpeg",
            "deutsche bank" : "deutschebank.jpeg",
            "postbank" : "postbank.jpeg",
            "shell" : "shell.jpeg",
            "aral" : "aral.jpeg",
            "jet" : "jet.jpeg",
            "esso" : "esso.jpeg",
            "omv" : "omv.jpeg",
            "Bank" : "Bank.png",
            "Tankstellen" : "Tankstellen.png",
            "Unterhaltung" : "Unterhaltung2.png",
            "Supermarkt" : "Supermarkt.png",
            "agip" : "agip.png",
            "Bars" : "Bars.png",
            "Kinos" : "Kinos.png",
            "Discotheken" : "Discotheken.png",
            "Museen" : "Museen.png",
            "Cafes" : "Cafes.png",
            "UBahn-Stationen" : "ubahn.png",
            "Parkplätze": "parking.png",
            "Gesundheit": "Gesundheit.png",
            "Apotheke": "Apotheke.png",
            "Arzt": "Arzt.png",
            "Krankenhaus": "Krankenhaus.png",
            "Fitness-Studio": "Gym.png",
            "Taxi-Stände": "Taxi.png",
            "Autohändler": "Autohaendler.png",
            "Mode": "Mode.png",
            "Baumärkte": "Baumaerkte.png",
            "Schuhläden" : "Schuhlaeden.png",
            "Haustiere": "Haustiere.png",
            "Schmuckhändler": "Schmuckhaendler.png",
            "Einkaufscenter": "Einkaufscenter.png",
            "Shoppen": "Shoppen.png"
            
            
]
    
   
    
}

