//
//  ArrayExtensions.swift
//  iOSGruppe7
//
//  Created by Felix

//Permutationen generieren

//https://gist.githubusercontent.com/proxpero/9fd3c4726d19242365d6/raw/62b2ed63dc3f8d4e3661e05d22c81f292cef56c9/Permutations.swift
import Foundation
extension Array {
    func decompose() -> (Generator.Element, [Generator.Element])? {
        guard let x = first else { return nil }
        return (x, Array(self[1..<count]))
    }
}

func between<T>(x: T, _ ys: [T]) -> [[T]] {
    guard let (head, tail) = ys.decompose() else { return [[x]] }
    return [[x] + ys] + between(x : x, tail).map { [head] + $0 }
}


func permutations<T>(xs: [T]) -> [[T]] {
    guard let (head, tail) = xs.decompose() else { return [[]] }
    return permutations(xs: tail).flatMap { between(x : head, $0) }
}

