//
//  HardcodedViewController.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 13.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class HardcodedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //OUTLETS
    
    @IBOutlet weak var background: UIImageView!
    
    @IBOutlet weak var hardcodedTableView: UITableView!
    
    var identities = [String]()
    public static var zellen = [String]()
    let viewcontroller = ViewController()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        identities = ["A", "B", "C", "D"]
        
        // Do any additional setup after loading the view.
        hardcodedTableView.backgroundView = background
    }
    
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (HardcodedViewController.zellen.count)
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DynamicViewControllerCell
        let clicked = HardcodedViewController.zellen[indexPath.row]
        
        cell.myImage.image = UIImage(named: ImageDictionary.imageDictionary[clicked]!)
        cell.myLabel.text = clicked
        
        
        
        
        //cell.textLabel?.text = keys[indexPath.row]
        return(cell)    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var output : [Result] = []
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! DynamicViewControllerCell
        
        
        
        let dictionaryInput = PlacesDictionary.translateIntoEnum(input: currentCell.myLabel.text!)
        let dictionaryInputValue = dictionaryInput[0]
        let neueFilterliste : [String] = []
        
        
        print(dictionaryInputValue)
        print(neueFilterliste)
        print(MapManager.getCoordinate())
        
        GooglePlacesWebAPI.searchTypeWithKeywords(type: dictionaryInput[0], keywordSet: [], location: MapManager.getCoordinate(), radius: 50000, onResultReadyCallback: {(resultSet: [String : [Result]]) in
            for (key, results) in resultSet {
                output.append(contentsOf: results)
            }
            print(resultSet.count)
            for results in resultSet.values {
               print(output[0])
               // output.append(results[0])
                
            }
            ViewController.mapManager.displayGooglePlacesResult(results: output)
            
            
        })
        
        print(output.count)
        print("-----------------------")
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}




