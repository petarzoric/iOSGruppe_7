//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation
import GoogleMaps

//Protocol für Kommunikation MapManager -> UI
protocol MapManagerProtocol
{
    func markerSelected(marker : GMSMarker, correspondingResult : Result)
    func routesLoaded(routes : [Route])
    func onMyLocationButtonClicked()
    func shortestRoutesCalculated(marker : [GMSMarker])
}
