//
//  DynamicViewControllerCell.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 29.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//


/*
 Erweiterung, um Custom Cells anzeigen können, in dem Fall mit nem Image und Text; würde mit standard-cells nicht gehen in Tableviews.
 Wird in der Preference View, Third View, Hardcoded View und Dynamic View verwendet.
*/

import UIKit

class DynamicViewControllerCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myLabel: UILabel!
   
    
    @IBOutlet weak var labelDescription: UILabel!
    
    @IBOutlet weak var labelNumber: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        layer.backgroundColor = UIColor.clear.cgColor
        backgroundColor = contentView.backgroundColor
        
        // Configure the view for the selected state
    }

}
