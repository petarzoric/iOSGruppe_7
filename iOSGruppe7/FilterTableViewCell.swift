//
//  filterTableViewCell.swift
//  iOSGruppe7
//
//  Created by admin on 23.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import Foundation
import UIKit

class FilterTableViewCell: UITableViewCell {
    @IBOutlet weak var filterCellImage: UIImageView!
    
    @IBOutlet weak var filterCellLabel: UILabel!
    @IBOutlet weak var filterCellLabelzwo: UILabel!
}

