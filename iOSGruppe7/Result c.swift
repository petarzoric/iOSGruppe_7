

import UIKit
import CoreData

class Markers: NSManagedObject {
    
    @NSManaged var locationX: Int
    @NSManaged var locationY: Int
    @NSManaged var name : String
    @NSManaged var imageUrl : String
    @NSManaged var placeId : String
    @NSManaged var addressName : String
    @NSManaged var hashv : Int
}
