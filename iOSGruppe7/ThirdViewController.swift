    //
//  ThirdViewController.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 10.07.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.

/*
 Third View, weil das zum damaligen Zeitpunkt unsere dritte View war lol
 Passender wäre sowas wie FilterView.
 Ist eben die View, die als erstes erreicht wird, sobald der User, den Filter-Button oben links betätigt.
 Enthält eben die ersten "Vorfilter", die dann noch weiter kategorisiert werden.
 */




import UIKit
import Foundation
import CoreLocation

class ThirdViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //OUTLETS
    
    let viewController = ViewController()
    
    
    var identities = [String]()
    
    @IBOutlet weak var thirdTableView: UITableView!
    
    /*Hier wird unsere aller Erste Filterliste festgelegt, erstmal hardgecoded
     
     */
    let FilterListeLvl1 = ["Supermarkt", "Unterhaltung", "Tankstellen", "Shoppen", "Taxi-Stände", "Bank", "Gesundheit", "Fitness-Studio", "UBahn-Stationen", "Parkplätze"]
    //gleich große liste mit places anlegen. Googleabfrage hat norma etc. geliefert und danach können wir filtern.
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*  hier werden die Indentities festgelegt. Diese müssen in der richtigen Reihenfolge stehen, um das clickbyindex-Zeug unten realisieren zu können, da dort die richtige Indexnummer der Zeilen gebraucht wird.
         Ggf TODO: das Ganze in ein Dictionary packen, da es a) Fehler vermeidet und b) modularer wird.
         
         */
        identities = ["DynamicView", "HardcodedView", "DynamicView", "HardcodedView", "DynamicView", "DynamicView", "HardcodedView", "DynamicView", "DynamicView","DynamicView"]
        
        
    }
    
    
    
    /*
     Hier bekommen wird einfach die Anzahl der Items in der Filterliste. Das legt entsprechend
     die Anzahl unserer Zeilen in der Tabelle fest.
     Ggf TODO: Höhe der Zeilen je nach Anzahl der Zeilen ändern, um die Fläche besser ausnutzen zu
     können
     
     */
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (FilterListeLvl1.count)
        
    }
    
    /*
     Hier wird die Tabelle mit den Einträgen unserer Filterliste befüllt.
     */
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DynamicViewControllerCell
        let clicked = FilterListeLvl1[indexPath.row]
        
        cell.myImage.image = UIImage(named: ImageDictionary.imageDictionary[clicked]!)
        cell.myLabel.text = clicked
        
        
        
        
        //cell.textLabel?.text = keys[indexPath.row]
        return(cell)    }
    
    /*
     Hier wird der Click des Users auf eine Zelle detected und weiterverarbeitet.
     
     */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! DynamicViewControllerCell
        
        
        /*
         translatetoEnum bekommt den String-Wert der geklickten Zelle und gibt uns den dazugehörigen PlaceType zurück, allerdings in ein Array gepackt, da ggf. mehrere Placetypes zurückgegeben werden könnten je nach Auswahl, @see PlacesDictionary.swift
         
         
         */
        let dictionaryInput = PlacesDictionary.translateIntoEnum(input: currentCell.myLabel.text!)
        
        print(dictionaryInput)
        
        /*
         Hier fragen wir erst mal ab, wie viele Werte im Array waren, sprich ob die Anfrage einen
         oder mehrere Placetypes erfordert.
         
         */
        var type = currentCell.myLabel.text!
        var output : [Result] = []
        if(type == "UBahn-Stationen" || type == "Parkplätze" || type == "Fitness-Studio" || type == "Taxi-Stände"){
            
            GooglePlacesWebAPI.searchTypeWithKeywords(type: dictionaryInput[0], keywordSet: [], location: MapManager.getCoordinate(), radius: 50000, onResultReadyCallback: {(resultSet: [String : [Result]]) in
                for (key, results) in resultSet {
                    output.append(contentsOf: results)
                }
                print("Callback")
                
                for results in resultSet.values {
                    print(results[0].name)
                    //output.append(results[0])
                }
                
                
                ViewController.mapManager.displayGooglePlacesResult(results: output)
                
                
            })
            for result in output {
                print (result)
            }
            self.navigationController?.popToRootViewController(animated: true)
            
            
        }
            
        else {
            
            if(dictionaryInput.count > 1){
                print(currentCell.myLabel.text!)
                switch currentCell.myLabel.text! {
                    
                case "Shoppen":
                    HardcodedViewController.zellen = [ "Mode", "Schuhläden", "Schmuckhändler", "Einkaufscenter", "Baumärkte", "Autohändler" ,"Haustiere"]
                    print("-------------")
                    let vcName = identities[(indexPath?.row)!]
                    let viewController = storyboard?.instantiateViewController(withIdentifier: vcName)
                    self.navigationController?.pushViewController(viewController!, animated: true)
                    break
                    
                case "Gesundheit":
                    print("Spring in Gesundheit Case")
                    HardcodedViewController.zellen = ["Arzt", "Apotheke", "Krankenhaus"]
                    let vcName = identities[(indexPath?.row)!]
                    
                    let viewController = storyboard?.instantiateViewController(withIdentifier: vcName)
                    self.navigationController?.pushViewController(viewController!, animated: true)
                    break
                    
                case "Unterhaltung":
                    print("Springe in den Case Unterhaltung")
                    /*
                     hier wird die neue View initialisiert für den Fall, dass der User auf die Zelle
                     Unterhaltung klickt
                     */
                    HardcodedViewController.zellen = ["Bars", "Kinos", "Discotheken", "Museen", "Cafes"]
                    let vcName = identities[(indexPath?.row)!]
                    
                    
                    let viewController = storyboard?.instantiateViewController(withIdentifier: vcName)
                    self.navigationController?.pushViewController(viewController!, animated: true)
                    break
                    
                    //TODO: weitere cases hinzufügen, wo wir direkt statische Werte für die nächste View setzen können
                    
                    
                    
                default:
                    print("default-fall")
                    
                }
                
            }
            else {
                // da wir wissen, dass nur ein Wert im array ist, entpacken wir den erst mal.
                let dictionaryInputValue = dictionaryInput[0]
                print(dictionaryInputValue)
                /*
                 wir fragen unser Dictionary nach den Keywords für den Placetype und speichern
                 das als unsere neue Keywordliste
                 */
                let neueFilterliste = PlacesDictionary.dictionary[dictionaryInputValue]!
                print(neueFilterliste)
                
                let loc = CLLocationCoordinate2D(latitude: 48.137154, longitude: 11.576124)
                
                GooglePlacesWebAPI.searchTypeWithKeywords(type: dictionaryInputValue, keywordSet: neueFilterliste, location: MapManager.getCoordinate(), radius: 50000, onResultReadyCallback: {(resultSet: [String : [Result]]) in
                    for (key, results) in resultSet {
                    }
                    
                    print("Callback")
                    let keys = [String] (resultSet.keys)
                    DynamicViewController.FilterListeLvl1 = neueFilterliste
                    neueViewPushen(filterliste: resultSet)
                    
                    
                })
                
            }
        }
        
        func neueViewPushen(filterliste: [String: [Result]]){
            print("neue view")
            
            DynamicViewController.resultList = filterliste
            
            let vcName = identities[(indexPath?.row)!]
            
            // https://www.youtube.com/watch?v=yupIw9FXUso
            /*Hier wird festgelegt auf welchen Viewcontroller wir bei welchem Zellenclick switchen
             a) Dynamischer Controller: wir schauen uns die Ergebnisse der PlacesAPI an und zeigen nur bestimmte Zellen an, hat zb Rewe keinen Treffer in der Umgebung des Users angezeigt,
             brauchen wir keine Zelle für Rewe anzeigen
             
             b) Hardcoded Controller: bei manchen Zellen wie beispielsweise Unterhaltung macht es Sinn,
             den Placetype noch weiter auf andere Placetypes zu filtern, bei Unterhaltung entsprechend Kino, Bars usw. Das machen wir dann aber hardcoded.
             TODO: macht das überhaupt Sinn? Es kann ja auch sein, dass in der Umgebung des Users keine Bars/Kinos zu finden sind -> brauchen theoretisch den Button nicht anzeigen.
             
             */
            
            let viewController = storyboard?.instantiateViewController(withIdentifier: vcName)
            self.navigationController?.pushViewController(viewController!, animated: true)
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
