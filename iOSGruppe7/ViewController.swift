//
//  ViewController.swift
//  iOSGruppe7
//
//  Created by Petar Zoric on 06.06.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit
import CoreData

//T

class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MapManagerProtocol {
    

    //outlets
    @IBOutlet weak var googleMapsView: GMSMapView!
    @IBOutlet weak var filterTableview: UITableView!//FilterTableView initialisiert
    
    //variables
    static var mapManager = MapManager()
    private var locationManager = CLLocationManager()
    private var coreData = CoreDataManager(entityName: "UserPreferences")
    private var selectedMarker = [GMSMarker]()
    private var routes = [Route]()
    private var myLocationIsIncludedInRoute = false
    
    @IBOutlet weak var test: UIView!
    //actions
    


    

   
    //---------------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  NotificationCenter.default.addObserver(self, selector: #selector(goToDifferentView), name: NSNotification.Name(rawValue: "segue"), object: nil)
        
       // 
       //  CoreDataManager.deleteAllData()
        
        
        ViewController.mapManager.setMap(googleMap : googleMapsView)
        ViewController.mapManager.setUiCallback(uiCallback : self)
        
        
        initLocationManager()
        coreData.showPreferenceData()

        filterTableview.setEditing(true, animated: true)
        
    }
    
    func initLocationManager()
    {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
        locationManager.requestAlwaysAuthorization()
    }

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while get location\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        self.googleMapsView.isMyLocationEnabled = true
        let location = locations.last
        let camera  = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        
        self.googleMapsView.animate(to: camera)
        self.locationManager.stopUpdatingLocation()
    }
 
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace)
    {
        let marker = ViewController.mapManager.addAutoCompleteResult(placeResult: place)
        selectedMarker.append(marker)
        ViewController.mapManager.setRoute(markers: selectedMarker)
        filterTableview.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("ERROR AUTO COMPLETE \(error)")
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openSearchAddress(_ sender: UIBarButtonItem) {
        let autoCompleteController = GMSAutocompleteViewController()
        autoCompleteController.delegate = self
        self.locationManager.startUpdatingLocation()
        self.present(autoCompleteController, animated: true, completion: nil)
    }
    
    /* Patrick Nagel: Funktion um die Filtertableliste dynamisch anzupassen. Leider musste die Funktion kurzfristig raus genommen werden, da die View, welche die Filtertableview beinhaltet, sich über die Filtertableview geschoben hat (Ab Element 3). Dieser Bug konnte mit erneutem Aufrufen der Funktion behoben werden, dieser Aufruf musste allerdings manuell via button aufgerufen werden. Aus diesen Gründen wurde der Code entfernt 23:57
    
    @IBAction func organizeButton(_ sender: Any) {
        showHideFilterTableView()
    }
    
    
    func showHideFilterTableView()
    {
        filterTableview.reloadData()
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height
        
        UITableView.animate(withDuration: 0.0, delay: 1.0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.filterTableview.reloadData()
            
            self.test.layoutIfNeeded()
            self.filterTableview.layoutIfNeeded()
            
            self.filterTableview.frame = CGRect(x: 0, y: 0, width: w, height: self.filterTableview.contentSize.height)
            self.googleMapsView.frame = CGRect(x: 0, y: -44 - self.filterTableview.contentSize.height, width: w, height: h)
            self.test.frame = CGRect(x: 0, y: h - 44 - self.filterTableview.contentSize.height, width: w, height: self.filterTableview.contentSize.height)
            self.test.layoutIfNeeded()
            self.filterTableview.layoutIfNeeded()
            
            self.filterTableview.reloadData()
            
        } )
        filterTableview.reloadData()
        
        
    }
*/
    
    
    
    
    //----------------------MapManager--------------------------
    
    //Mapmanager Marker ausgewählt
    func markerSelected(marker: GMSMarker, correspondingResult : Result) {
        selectedMarker.append(marker)
        if selectedMarker.count != 0
        {
            showRouteModePicker(toMarker: marker)
        }

        filterTableview.reloadData()
        
    }
    
    //Mapmanger routen wurden geladen
    func routesLoaded(routes : [Route])
    {
        self.routes = routes
        filterTableview.reloadData()
    }
    

    
    //Auf Ortungsbutton der Map geclicked
    func onMyLocationButtonClicked()
    {
        if !myLocationIsIncludedInRoute
        {
            myLocationIsIncludedInRoute = true
            filterTableview.reloadData()
        }
        
    }
    @IBAction func shortestRouteButtonAction(_ sender: Any) {
        
        if (selectedMarker.count > 6 && !myLocationIsIncludedInRoute) || (selectedMarker.count > 5 && myLocationIsIncludedInRoute)
        {
            let alert = UIAlertController(title: "Zu viele Stopps", message: "Deine Route enthält zu viele Punkte für die Berechnung der kürzesten Route. Für diese Funktion dürfen maximal 6 Punkte enthalten sein.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: {
                    
                })
            }))
            
            DispatchQueue.main.async {
                self.present(alert, animated : true, completion: {})
            }
        }
        else{
            ViewController.mapManager.calculateShortestRoute()
        }
    }
    
    func shortestRoutesCalculated(marker : [GMSMarker])
    {
        selectedMarker = marker
        filterTableview.reloadData()
    }
    
    //----------------------RouteMode Picker----------------------- 
    //Felix
    
    func showRouteModePicker(toMarker : GMSMarker)
    {
        ViewController.mapManager.addLastRouteMode(routeMode: routeModeData[lastSelectedRouteModeIndex].routeMode)
        
        let viewController = UIViewController()
        let w = 250
        let h = 100
        viewController.preferredContentSize = CGSize(width : w, height : h)
        
        let picker = UIPickerView(frame : CGRect(x: 0, y: 0, width : w, height: h))
        picker.dataSource = self
        picker.delegate = self
        picker.selectRow(lastSelectedRouteModeIndex, inComponent: 0, animated: true)
        
        viewController.view.addSubview(picker)
        
        let alert = UIAlertController(title: "Navigationsmodus auswählen", message: "Ziel: " + toMarker.title!, preferredStyle: .actionSheet)
        alert.setValue(viewController, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
                alert.dismiss(animated: true, completion: {
                    let index = picker.selectedRow(inComponent: 0)
                    let mode = self.routeModeData[index].routeMode
                    self.filterTableview.reloadData()

            })
        }))

        
        alert.view.addConstraint(NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: picker, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: picker, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: 0))
        
        DispatchQueue.main.async {
            self.present(alert, animated : true, completion: {})
        }
    }
    

    var routeModeData : [(clearText : String, routeMode : RouteMode)] =
        [
            (clearText : "Zu Fuß", routeMode : RouteMode.Walking),
            (clearText : "Fahrrad fahren", routeMode : RouteMode.Bicycle),
            (clearText : "Mit dem Auto", routeMode : RouteMode.Default),
            (clearText : "Öffentlich", routeMode : RouteMode.Transit)
    ]
    
    func numberOfComponents(in pickerView : UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView : UIPickerView, numberOfRowsInComponent component : Int) -> Int
    {
        return routeModeData.count
    }
    
    func pickerView(_ pickerView : UIPickerView, titleForRow row : Int, forComponent component : Int) -> String?
    {
        return routeModeData[row].clearText
    }
    
    var lastSelectedRouteModeIndex = 0
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        lastSelectedRouteModeIndex = row
        ViewController.mapManager.setLastRouteMode(routeMode: routeModeData[row].routeMode)
    }


    //---------------------------Tableview-------------------------------------------------
    //Felix/Patrick
    
    //Anzahl der elemente
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if myLocationIsIncludedInRoute
        {
            return selectedMarker.count + 1 // + 1 für Mein Standort
        }
        return selectedMarker.count
    }
    
    //Item an stelle indexPath löschbar oder nicht
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
       /*
        if indexPath.row == 0
        {
            if(clickedMyLocationButton)
            {
                return UITableViewCellEditingStyle.none
            }
        }
 */
        
        return UITableViewCellEditingStyle.delete
    }
    
    //Items tauschen
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        // swap mit sich selbst funktioniert nicht, mit guard handeln
        
        let rowSrc = sourceIndexPath.row
        let rowDest = destinationIndexPath.row

        guard rowSrc == 0 || rowDest == 0 else
        {
            guard rowSrc == rowDest else {
                if myLocationIsIncludedInRoute
                {
                    swap(&selectedMarker[rowSrc-1],&selectedMarker[rowDest-1])
                    ViewController.mapManager.swapRouteModes(source: rowSrc - 1, dest: rowDest - 1)
                }
                else
                {
                    swap(&selectedMarker[rowSrc],&selectedMarker[rowDest])
                    ViewController.mapManager.swapRouteModes(source: rowSrc, dest: rowDest)
                }
                tableView.reloadData()
                ViewController.mapManager.setRoute(markers: selectedMarker)
                
                return
            }
            tableView.reloadData()
            ViewController.mapManager.setRoute(markers: selectedMarker)
            return
        }
        tableView.reloadData()
        ViewController.mapManager.setRoute(markers: selectedMarker)

    }
    
    //Item gelöscht
    func tableView(_ tableView : UITableView, commit editingStyle : UITableViewCellEditingStyle, forRowAt indexPath : IndexPath)
    {
        if editingStyle == .delete
        {
            let row = indexPath.row
            
            if myLocationIsIncludedInRoute
            {
                if selectedMarker.count > 0
                {

                    if row == 0 {
                        ViewController.mapManager.setIncludeMyLocation(include: false)
                        myLocationIsIncludedInRoute = false
                    }
                    else
                    {
                        ViewController.mapManager.removeRouteMode(index: row - 1)
                        selectedMarker.remove(at: row - 1)
                        if routes.count > 0
                        {
                            routes.remove(at: row-1)
                        }
                    }
                   
                }
                else
                {
                    ViewController.mapManager.setIncludeMyLocation(include: false)
                    myLocationIsIncludedInRoute = false
                }
            }
            else
            {
                ViewController.mapManager.removeRouteMode(index: row)
                selectedMarker.remove(at: row)
                if routes.count > 0
                {
                     routes.remove(at: row)
                }
               
            }
            


            ViewController.mapManager.setRoute(markers: selectedMarker)
            filterTableview.reloadData()
        }
    }
    
    
    //Item an stelle indexPath generieren
    public func tableView(_ tableView: UITableView, cellForRowAt tindexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filtercell", for:tindexPath) as! FilterTableViewCell
        
        let row = tindexPath.row
        
        if myLocationIsIncludedInRoute
        {
            if tindexPath.row == 0
            {
                cell.filterCellLabel?.text = "Mein Standort"
                cell.filterCellLabelzwo?.text = "0m"
                cell.filterCellImage?.image = UIImage(named: "MyLocation")
            }
            else
            {
                let marker = selectedMarker[row - 1]
                let result = marker.userData as! Result
                
                cell.filterCellLabel?.text = result.name
                cell.filterCellLabelzwo?.text = (((routes.count > 0 ) && (row <= routes.count)) ? routes[row-1].duration + " " + routes[row-1].distance : result.distanceString)

                cell.filterCellImage?.image = marker.icon
            }
        }
        else
        {
            let marker = selectedMarker[row]
            let result = marker.userData as! Result
            
            cell.filterCellLabel?.text = result.name
            cell.filterCellLabelzwo?.text = (row > 0) ? (((routes.count > 0 ) && (row < routes.count)) ? routes[row].duration + " " + routes[row].distance : "") : result.distanceString

            cell.filterCellImage?.image = marker.icon
        }

        return cell
    }
    
    //Item an stelle indexPath ausgewälht
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        let cell = tableView
        if selectedMarker.count > 0
        {
            ViewController.mapManager.onFilterSelected(item : selectedMarker[(indexPath?.item)!])
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }


    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){
        
    }


}

