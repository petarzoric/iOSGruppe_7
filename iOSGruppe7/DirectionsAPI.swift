//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//
import Foundation
import CoreLocation
import Alamofire
import SwiftyJSON
import GoogleMaps

//Lädt route zwischen zwei Koordinaten von GoogleDirectionsAPI
class DirectionsAPI
{
    
    public static func loadGoogleDirection(startLocation : CLLocationCoordinate2D, endLocation : CLLocationCoordinate2D, routeMode : RouteMode, routesLoadedCallback : @escaping (([Route]) -> ()))
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=\(routeMode.rawValue)&key=AIzaSyBNqFS8s8q61_sy8cGsglnlUa3I8HxarFQ"
        
        Alamofire.request(url).responseJSON {response in
            
            let json = JSON(data: response.data!)
            let routes = json["routes"].arrayValue
            
            var result = [Route]()
            
            for route in routes {
                let routeOverviewPolyLine = route["overview_polyline"].dictionary
                let points = routeOverviewPolyLine?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyLine = GMSPolyline.init(path: path)
                polyLine.strokeWidth = 4
                
                let legs = route["legs"].arrayValue
                let distanceObj = legs.first!["distance"]
                let durationObj = legs.first!["duration"]
                
                let distance = distanceObj["text"].stringValue
                let duration = durationObj["text"].stringValue
                
                let route = Route(duration : duration, distance : distance, routeMode : routeMode, mapsRoute : polyLine)
                
                result.append(route)
            }
            DispatchQueue.main.async
                {
                    routesLoadedCallback(result)
            }
        }
    }
    
    //Aufruf für Kürzeste route um permutationsindex zu behalten
    public static func loadGoogleDirection(startLocation : CLLocationCoordinate2D, endLocation : CLLocationCoordinate2D, routeMode : RouteMode, forPermutationIndex : Int, routesLoadedCallback : @escaping ((Int, [Route]) -> ()))
    {
        let origin = "\(startLocation.latitude),\(startLocation.longitude)"
        let destination = "\(endLocation.latitude),\(endLocation.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=\(routeMode.rawValue)&key=AIzaSyBNqFS8s8q61_sy8cGsglnlUa3I8HxarFQ"
        
        Alamofire.request(url).responseJSON {response in
            
            let json = JSON(data: response.data!)
            let routes = json["routes"].arrayValue
            
            var result = [Route]()
            
            for route in routes {
                let routeOverviewPolyLine = route["overview_polyline"].dictionary
                let points = routeOverviewPolyLine?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyLine = GMSPolyline.init(path: path)
                polyLine.strokeWidth = 4
                
                let legs = route["legs"].arrayValue
                let distanceObj = legs.first!["distance"]
                let durationObj = legs.first!["duration"]
                
                let distance = distanceObj["text"].stringValue
                let duration = durationObj["text"].stringValue
                
                let route = Route(duration : duration, distance : distance, routeMode : routeMode, mapsRoute : polyLine)
                
                result.append(route)
            }
            DispatchQueue.main.async
                {
                    routesLoadedCallback(forPermutationIndex, result)
            }
        }
    }
}
