//
//  Section.swift
//  iOSGruppe7
//
//  Created by admin on 28.06.17.
//  Copyright © 2017 Petar Zoric. All rights reserved.
//

import Foundation

struct Section {
    var genre: String!
    var subgenre: [String]
    var expanded: Bool!
    
    init(genre: String, subgenre: [String], expanded: Bool) {
        self.genre = genre
        self.subgenre = subgenre
        self.expanded = expanded
    }
}
