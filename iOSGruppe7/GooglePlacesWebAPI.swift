//
//
//  iOSGruppe7
//
//  Created by Felix Frömel
//  Copyright © 2017 Felix Frömel. All rights reserved.
//

import CoreLocation
import Foundation
import MapKit

//Lädt Ergebnisse von der GooglePlacesAPI
class GooglePlacesWebAPI
{
    static let BASEURL = "https://maps.googleapis.com/maps/api/place"
    static let NEARBY_URL = BASEURL + "/nearbysearch/json?"
    static let TEXTSEARCH_URL = BASEURL + "/textsearch/json?query="
    
    static let KEY = "&key=AIzaSyAhJ4aloMRsSvN4FyFlO41dTKRHo28nWWg"
    
    static let LOCATION = "location="
    static let RADIUS = "&radius="
    static let TYPE = "&type="
    static let NAME = "&name="
    static let KEYWORD = "&keyword="
    
    
    static func searchByName(name : String, location : CLLocationCoordinate2D = MapManager.getCoordinate(), radius : Int = 50000, onResultReadyCallback : @escaping ([Result]) -> ())
    {
        let urlString  = buildUrlString(location: location, name: name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, radius : radius)

        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: urlString)
        var req = URLRequest(url: url!)
        req.httpMethod = "GET"
        
        var resultSet = [Result]()
        
        let task = session.dataTask(with: req, completionHandler:
        {
            (data, response, error) in
            
            if error != nil {
                print("error")
                print(error!.localizedDescription)
                
                DispatchQueue.main.async {
                    onResultReadyCallback(resultSet)
                    
                }
            }
            else
            {
                do
                {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    {
                        let nextPageToken = json["next_page_token"]
                        
                        let results = json["results"] as? Array<NSDictionary>
                        for result in results! {
                            let name = result["name"] as! String
                            let id = result["id"] as! String
                            let addressName = result["vicinity"] as! String
                            let imageUrl : String = ""
                            var placeLocation : CLLocationCoordinate2D!
                            
                            if let geometry = result["geometry"] as? NSDictionary {
                                if let location = geometry["location"] as? NSDictionary {
                                    let lat = location["lat"] as! CLLocationDegrees
                                    let long = location["lng"] as! CLLocationDegrees
                                    placeLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                    let placeResult = Result(location: placeLocation, name : name, placeId : id, addressName : addressName, imageURL : imageUrl)
                                    
                                    resultSet.append(placeResult)
                                    
                                }
                            }
                        }
                        
                    }
                }
                catch
                {
                    print("error in JSONSerialization")
                }
            }
            
            DispatchQueue.main.async {
                onResultReadyCallback(resultSet)
                
            }
        })
        
        task.resume()
    }
    
    static func searchByKeyword(keyword : String, location : CLLocationCoordinate2D = MapManager.getCoordinate(), radius : Int = 50000, onResultReadyCallback : @escaping ([Result]) -> ())
    {
        let urlString  = buildUrlString(location: location, keyword: keyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, radius : radius, rankByDistance : true)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: urlString)
        var req = URLRequest(url: url!)
        req.httpMethod = "GET"
        
        var resultSet = [Result]()
        
        let task = session.dataTask(with: req, completionHandler:
        {
            (data, response, error) in
            
            if error != nil {
                print("error")
                print(error!.localizedDescription)
                
                DispatchQueue.main.async {
                    onResultReadyCallback(resultSet)
                    
                }
            }
            else
            {
                do
                {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    {
                        let results = json["results"] as? Array<NSDictionary>
                        for result in results! {
                            let name = result["name"] as! String
                            let id = result["id"] as! String
                            let addressName = result["vicinity"] as! String
                            let imageUrl : String = ""
                            var placeLocation : CLLocationCoordinate2D!
                            
                            if let geometry = result["geometry"] as? NSDictionary {
                                if let location = geometry["location"] as? NSDictionary {
                                    let lat = location["lat"] as! CLLocationDegrees
                                    let long = location["lng"] as! CLLocationDegrees
                                    placeLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                    let placeResult = Result(location: placeLocation, name : name, placeId : id, addressName : addressName, imageURL : imageUrl)
                                    
                                    resultSet.append(placeResult)
                                    
                                }
                            }
                        }
                    }
                }
                catch
                {
                    print("error in JSONSerialization")
                }
            }
            
            DispatchQueue.main.async {
                onResultReadyCallback(resultSet)
            }
        })
        task.resume()
    }
    
    
    
    static func searchTypeWithKeywords(type : PlacesType, keywordSet : [String] = [], location : CLLocationCoordinate2D = MapManager.getCoordinate(), radius : Int = 50000, onResultReadyCallback : @escaping ([String : [Result]]) -> ())
    {
        let urlString  = buildUrlString(location: location, type: type, radius : radius, rankByDistance : true)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: urlString)
        var req = URLRequest(url: url!)
        req.httpMethod = "GET"
        
        var keywordResultsetDictionary =  [String : [Result]]()
        
        let task = session.dataTask(with: req, completionHandler:
        {
            (data, response, error) in
            
            if error != nil {
                print("error")
                print(error!.localizedDescription)
                
                DispatchQueue.main.async {
                    onResultReadyCallback(keywordResultsetDictionary)
                    
                }
            }
            else
            {
                do
                {
                    var nextPageToken : String?
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                    {
                        nextPageToken = json["next_page_token"] as? String
                        
                        let results = json["results"] as? Array<NSDictionary>
                        for result in results! {
                            let name = result["name"] as! String
                            let id = result["id"] as! String
                            let addressName = result["vicinity"] as! String
                            let imageUrl : String = ""
                            var placeLocation : CLLocationCoordinate2D!
                            
                            if let geometry = result["geometry"] as? NSDictionary {
                                if let location = geometry["location"] as? NSDictionary {
                                    let lat = location["lat"] as! CLLocationDegrees
                                    let long = location["lng"] as! CLLocationDegrees
                                    placeLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                    let placeResult = Result(location: placeLocation, name : name, placeId : id, addressName : addressName, imageURL : imageUrl)
                                    
                                    if keywordSet.count > 0
                                    {
                                        for keyword in keywordSet
                                        {
                                            if name.lowercased().contains(keyword)
                                            {
                                                if keywordResultsetDictionary[keyword] == nil {
                                                    keywordResultsetDictionary[keyword] = [Result]()
                                                }
                                                keywordResultsetDictionary[keyword]!.append(placeResult)
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if keywordResultsetDictionary[name] == nil {
                                            keywordResultsetDictionary[name] = [Result]()
                                        }
                                        keywordResultsetDictionary[name]!.append(placeResult)
                                    }
                                }
                            }
                        }
                        if nextPageToken != nil
                        {
                           nextTypeQuery(urlString: urlString, nextPageToken: nextPageToken!, previousResults: keywordResultsetDictionary, keywordSet: keywordSet, nextResultQueryCount: 0, onResultReadyCallback: onResultReadyCallback)
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                onResultReadyCallback(keywordResultsetDictionary)
                            }
                        }
                    }
                }
                catch
                {
                    print("error in JSONSerialization")
                    DispatchQueue.main.async {
                        onResultReadyCallback(keywordResultsetDictionary)
                    }
                }
            }
        })
        task.resume()
    }
    
    static func nextTypeQuery(urlString : String, nextPageToken : String, previousResults : [String : [Result]], keywordSet : [String], nextResultQueryCount : Int, onResultReadyCallback : @escaping ([String : [Result]]) -> ())
    {
        if nextResultQueryCount < 2
        {
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let url = URL(string: urlString + "&pagetoken=" + nextPageToken)
            var req = URLRequest(url: url!)
            req.httpMethod = "GET"
            
            
            let task = session.dataTask(with: req, completionHandler:
            {
                (data, response, error) in
                
                var newResults : [String : [Result]] = [String : [Result]]()
                
                for (key, value) in previousResults
                {
                    newResults[key] = value
                }
                
                if error != nil {
                    print("error")
                    print(error!.localizedDescription)
                    
                    DispatchQueue.main.async {
                        onResultReadyCallback(newResults)
                        
                    }
                }
                else
                {
                    do
                    {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        {
                            var hasNextToken = false
                            
                            if let pageToken = json["next_page_token"] as? String
                            {
                                hasNextToken = true
                            }
                            
                            let results = json["results"] as? Array<NSDictionary>
                            for result in results! {
                                let name = result["name"] as! String
                                let id = result["id"] as! String
                                let addressName = result["vicinity"] as! String
                                let imageUrl : String = ""
                                var placeLocation : CLLocationCoordinate2D!
                                
                                if let geometry = result["geometry"] as? NSDictionary {
                                    if let location = geometry["location"] as? NSDictionary {
                                        let lat = location["lat"] as! CLLocationDegrees
                                        let long = location["lng"] as! CLLocationDegrees
                                        placeLocation = CLLocationCoordinate2D(latitude: lat, longitude: long)
                                        let placeResult = Result(location: placeLocation, name : name, placeId : id, addressName : addressName, imageURL : imageUrl)
                                        
                                        if keywordSet.count > 0
                                        {
                                            for keyword in keywordSet
                                            {
                                                if name.lowercased().contains(keyword)
                                                {
                                                    if newResults[keyword] == nil {
                                                        newResults[keyword] = [Result]()
                                                    }
                                                    newResults[keyword]!.append(placeResult)
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if newResults["all"] == nil {
                                                newResults["all"] = [Result]()
                                            }
                                            newResults["all"]!.append(placeResult)
                                        }
                                    }
                                }
                            }
                            if hasNextToken
                            {
                                nextTypeQuery(urlString: urlString, nextPageToken: nextPageToken, previousResults: newResults, keywordSet: keywordSet, nextResultQueryCount: nextResultQueryCount + 1, onResultReadyCallback: onResultReadyCallback)
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                    onResultReadyCallback(newResults)
                                }
                            }
                        }
                    }
                    catch
                    {
                        print("error in JSONSerialization")
                        DispatchQueue.main.async {
                            onResultReadyCallback(previousResults)
                        }
                        
                    }
                }
            })
            task.resume()
        }
        else
        {
            DispatchQueue.main.async {
                onResultReadyCallback(previousResults)
            }
        }
    }
    
    
    
    
    static func buildUrlString(location : CLLocationCoordinate2D, type : PlacesType, radius : Int = 50000, rankByDistance : Bool = false, openNow : Bool = false) -> String
    {
        var url = "\(GooglePlacesWebAPI.NEARBY_URL)\(LOCATION)\(location.latitude),\(location.longitude)\(TYPE)\(type.rawValue)"
        
        if rankByDistance
        {
            url += "&rankby=distance"
        }
        else
        {
            url += "\(RADIUS)\(radius)"
        }
        
        
        if openNow
        {
            url += "&opennow"
        }
        
        url += KEY
        
        return url;
    }
    
    static func buildUrlString(location : CLLocationCoordinate2D, name : String, radius : Int = 50000, rankByDistance : Bool = false, openNow : Bool = false) -> String
    {
        var url = "\(GooglePlacesWebAPI.NEARBY_URL)\(LOCATION)\(location.latitude),\(location.longitude)\(NAME)\(name)"
        
        if rankByDistance
        {
            url += "&rankby=distance"
        }
        else
        {
            url += "\(RADIUS)\(radius)"
        }
        
        if openNow
        {
            url += "&opennow"
        }
        
        url += KEY
        
        return url;
    }
    
    static func buildUrlString(location : CLLocationCoordinate2D, keyword : String, radius : Int = 50000, rankByDistance : Bool = false, openNow : Bool = false) -> String
    {
        var url = "\(GooglePlacesWebAPI.NEARBY_URL)\(LOCATION)\(location.latitude),\(location.longitude)\(KEYWORD)\(keyword)"
        
        if rankByDistance
        {
            url += "&rankby=distance"
        }
        else
        {
            url += "\(RADIUS)\(radius)"
        }
        
        if openNow
        {
            url += "&opennow"
        }
        
        url += KEY
        
        return url;
    }
    
    static func buildUrlString(textQuery : String) -> String
    {
        var url = "\(GooglePlacesWebAPI.TEXTSEARCH_URL)\(textQuery)"
        
        url += KEY
        
        return url;
    }
}

